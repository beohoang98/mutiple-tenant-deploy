# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/gitlabhq/gitlab" {
  version     = "17.1.0"
  constraints = "17.1.0"
  hashes = [
    "h1:GqLEoWzj2HyfECNyFDg4qgPcX603p0YVR/DPIqq1ENs=",
    "zh:020e17565353a3bdad021d1e7599c72b825ec8bb3eb3fcb2f731267318fd89ed",
    "zh:029cdf75f3b6403d7d5ba227dccfad0e0764aec40119785c56f5c689eab6c379",
    "zh:1461a46fb66b6b5dfabe9fd7cc079cab451efe2c74064364dd157eb84dfccaaf",
    "zh:1b9b26fa8e39f0f7ae97857ffe9f123164c1e0539f6c06b552f7883e371cdc06",
    "zh:2ec16f03306a074ab6b7718e14f02651b1e0c0a096a632c2edf9a6ece3ba52dc",
    "zh:380012a066db6115b8b0afe8c8a6e0fcc4811a85875f9fe4aa64eb6a9be08634",
    "zh:47c2399e88df7e18b9ba6a548b23742d211ca6cc4b312f51dee3df90c4e62103",
    "zh:5cd0fd712cf80118c47af6dc94851a015fd5c8aec931339126731de0ada1e1a3",
    "zh:8ff031b3b7b402556c6a6fecc471ca47cca8ed1d90be13e552fae5cb0c012a8e",
    "zh:9c378b04e4935dce38bd67ec28bec409bbf381cf330807401ce71bb5dd0c11bb",
    "zh:aa19815d840c3277ee0debf18d5db00fcd585088b54899b956c8acca17d3a402",
    "zh:bcbb52c348d013f4b3f06c9f331127a5d71d551084ce85774b3e517d82befe99",
    "zh:cc06f50f590778d27fa55f2bc4f1d125e528c478aaab4954a3b9e6251234f581",
    "zh:f793836ad7ebd30bf65f7dcfa7e1357a05dcc43c1eef5b781273e27460793e17",
    "zh:f809ab383cca0a5f83072981c64208cbd7fa67e986a86ee02dd2c82333221e32",
  ]
}

provider "registry.terraform.io/hashicorp/local" {
  version = "2.5.1"
  hashes = [
    "h1:/GAVA/xheGQcbOZEq0qxANOg+KVLCA7Wv8qluxhTjhU=",
    "zh:0af29ce2b7b5712319bf6424cb58d13b852bf9a777011a545fac99c7fdcdf561",
    "zh:126063ea0d79dad1f68fa4e4d556793c0108ce278034f101d1dbbb2463924561",
    "zh:196bfb49086f22fd4db46033e01655b0e5e036a5582d250412cc690fa7995de5",
    "zh:37c92ec084d059d37d6cffdb683ccf68e3a5f8d2eb69dd73c8e43ad003ef8d24",
    "zh:4269f01a98513651ad66763c16b268f4c2da76cc892ccfd54b401fff6cc11667",
    "zh:51904350b9c728f963eef0c28f1d43e73d010333133eb7f30999a8fb6a0cc3d8",
    "zh:73a66611359b83d0c3fcba2984610273f7954002febb8a57242bbb86d967b635",
    "zh:78d5eefdd9e494defcb3c68d282b8f96630502cac21d1ea161f53cfe9bb483b3",
    "zh:7ae387993a92bcc379063229b3cce8af7eaf082dd9306598fcd42352994d2de0",
    "zh:9e0f365f807b088646db6e4a8d4b188129d9ebdbcf2568c8ab33bddd1b82c867",
    "zh:b5263acbd8ae51c9cbffa79743fbcadcb7908057c87eb22fd9048268056efbc4",
    "zh:dfcd88ac5f13c0d04e24be00b686d069b4879cc4add1b7b1a8ae545783d97520",
  ]
}
