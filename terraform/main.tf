terraform {
  required_providers {
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "17.1.0"
    }
  }
}

provider "gitlab" {
  token = var.gitlab_pat
}

data "gitlab_project" "demo_tenant" {
  path_with_namespace = "beohoang98/mutiple-tenant-deploy"
}

locals {
  environments = ["production", "staging", "development"]
  tenants      = ["devtest", "tenant1", "tenant2", "tenant3"]

  # The variables format is { "key": [{"Value", "Environment", "Tenant"}] }
  variables_json = jsondecode(data.local_file.variables.content)

  variables_list = flatten([
    for var_name, var_values in local.variables_json : [
      for var_value in var_values : {
        key         = var_name
        value       = var_value["Value"]
        environment = var_value["Environment"]
        tenant      = var_value["Tenant"]
      }
    ]
  ])
}

data "local_file" "variables" {
  filename = join("/", [path.module, "variables.json"])
}

resource "gitlab_project_variable" "vars" {
  for_each = {
    for idx, var in local.variables_list : idx => var
  }
  project           = data.gitlab_project.demo_tenant.id
  key               = each.value["key"]
  value             = each.value["value"]
  environment_scope = "${each.value["environment"]}/${each.value["tenant"]}"
  protected         = false
}
